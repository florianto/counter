## Counter | Application développé en React Native

Version basique d'une application qui compte pour vous grace à deux bouton + et -.

Le nom et la clé peut être changé dans le code (pour le moment), dans 'app/screens/Home.hs' l10 l11.

#### Modification envisageable:

- Modification du titre et de la clé sur l'application.
- Pouvoir compter plusieurs variables.

#### Screenshot v1

![Screenshot](screenshot/v1.png)
