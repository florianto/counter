import React from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
  AsyncStorage
} from "react-native";

const NAME = "Shishas fumées";
const key = "shisha";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = { count: 0 };
    this.retrieveData();
  }

  onPressPlus = () => {
    this.setState({
      count: this.state.count - -1
    });
    this.storeData(this.state.count - -1);
  };

  onPressMoins = () => {
    this.setState({
      count: this.state.count - 1
    });
    this.storeData(this.state.count - 1);
  };

  storeData = async data => {
    try {
      await AsyncStorage.setItem(key, data + "");
    } catch (error) {}
  };

  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        this.setState({
          count: value
        });
      }
    } catch (error) {}
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.counterContainer}>
          <Text style={styles.counter}>{this.state.count}</Text>
        </View>

        <View style={styles.nameContainer}>
          <Text style={styles.name}>{NAME}</Text>
        </View>

        <TouchableHighlight
          style={[styles.button, { backgroundColor: "#99FF66" }]}
          onPress={this.onPressPlus}
          underlayColor="#99FF66"
        >
          <Text style={{ fontSize: 70 }}>+</Text>
        </TouchableHighlight>

        <TouchableHighlight
          style={[styles.button, { backgroundColor: "#FF6666" }]}
          onPress={this.onPressMoins}
          underlayColor="#FF6666"
        >
          <Text style={{ fontSize: 70 }}>-</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  counterContainer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  counter: {
    fontSize: 100
  },
  nameContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  name: {
    fontSize: 30
  },
  button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default Home;
